-------------------------- Trees -----------------------

---- Pink Apple Leaves ----
minetest.register_node("add_trees:pink_apple_leaves", {
	description = "Pink Apple Tree Leaves",
	drawtype = "allfaces_optional",
	waving = 1,
	tiles = {"add_trees_pink_apple_leaves.png"},
	special_tiles = {"add_trees_pink_apple_leaves.png"},
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, leafdecay = 3, flammable = 2, leaves = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {'add_trees:pink_apple_sapling'}, rarity = 30},
			{items = {'add_trees:pink_apple_leaves'}}
		}
	},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})
		
---- Sapling Registration ----

local sapling_registration = function(node_name, descrip, texture, pos, height)

	minetest.register_node(node_name .."_sapling", {
	description = descrip .." Sapling",
	drawtype = "plantlike",
	tiles = {texture ..".png"},
	inventory_image = texture ..".png",
	wield_image = texture ..".png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = add_trees.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 2, dig_immediate = 3, flammable = 2,
		attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),
	on_construct = function(pos)
		minetest.get_node_timer(pos):start(math.random(30, 150))
	end,
	on_place = function(itemstack, placer, pointed_thing)
		itemstack = default.sapling_on_place(itemstack, placer, pointed_thing,
			node_name .."_sapling",
			-- minp, maxp to be checked, relative to sapling pos
			{x = -pos, y = 1, z = -pos},
			{x = pos, y = height, z = pos},
			-- maximum interval of interior volume check
			4)
		return itemstack
	end,
	})
end
	
sapling_registration("add_trees:emergent_apple", "Emergent Apple", "add_trees_emergent_apple_sapling", 3, 11)
sapling_registration("add_trees:ancient_emergent_apple", "Ancient Emergent Apple", "add_trees_ancient_emergent_apple_sapling", 5, 14)
sapling_registration("add_trees:conjoined_emergent_jungle", "Conjoined Emergent Jungle", "add_trees_conjoined_emergent_jungle_sapling", 12, 38)
sapling_registration("add_trees:emergent_acacia", "Emergent Acacia", "add_trees_emergent_acacia_sapling", 6, 12)
sapling_registration("add_trees:ancient_emergent_acacia", "Ancient Emergent Acacia", "add_trees_ancient_emergent_acacia_sapling", 12, 22)
sapling_registration("add_trees:conjoined_pine", "Conjoined Pine", "add_trees_conjoined_pine_sapling", 4, 23)
sapling_registration("add_trees:ancient_emergent_pine", "Ancient Emergent Pine", "add_trees_ancient_emergent_pine_sapling", 15, 44)
sapling_registration("add_trees:emergent_aspen", "Emergent Aspen", "add_trees_emergent_aspen_sapling", 2, 13)
sapling_registration("add_trees:conjoined_aspen", "Conjoined Aspen", "add_trees_conjoined_aspen_sapling", 3, 12)
sapling_registration("add_trees:pink_apple", "Pink Apple", "add_trees_pink_apple_sapling", 2, 9)
sapling_registration("add_trees:conjoined_pink_apple", "Conjoined Pink Apple", "add_trees_conjoined_pink_apple_sapling", 4, 10)	

-- register trees for leafdecay

default.register_leafdecay({
	trunks = {"default:tree"},
	leaves = {"add_trees:pink_apple_leaves"},
	radius = 3,
})
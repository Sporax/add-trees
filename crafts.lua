-- Fuel Registration --

local fuel_registration = function(reci, brtime)

	minetest.register_craft({
	type = "fuel",
	recipe = reci .."_sapling",
	burntime = brtime,
	})
end

fuel_registration("add_trees:emergent_apple", 5)
fuel_registration("add_trees:ancient_emergent_apple", 6)
fuel_registration("add_trees:conjoined_emergent_jungle", 7)
fuel_registration("add_trees:emergent_acacia", 6)
fuel_registration("add_trees:ancient_emergent_acacia", 7)
fuel_registration("add_trees:conjoined_pine", 6)
fuel_registration("add_trees:ancient_emergent_pine", 7)
fuel_registration("add_trees:emergent_aspen", 5)
fuel_registration("add_trees:conjoined_aspen", 6)
fuel_registration("add_trees:pink_apple", 3)
fuel_registration("add_trees:conjoined_pink_apple", 5)


-- Tree Recipe Registration --*

local emergent_tree_registration = function(otpt, node_reci)

	minetest.register_craft({
	output = otpt .."_sapling",
	recipe = {
		{node_reci, node_reci, ""},
		{node_reci, node_reci, ""},
		{"", "", ""},
	}
	})
end

emergent_tree_registration("add_trees:emergent_apple", "default:sapling")
emergent_tree_registration("add_trees:emergent_acacia", "default:acacia_sapling")
emergent_tree_registration("add_trees:emergent_aspen", "default:aspen_sapling")

local ancient_emergent_tree_registration = function(otpt, node_reci)

	minetest.register_craft({
	output = otpt .."_sapling",
	recipe = {
		{node_reci, node_reci, node_reci},
		{node_reci, node_reci, node_reci},
		{node_reci, node_reci, node_reci},
	}
	})
end

ancient_emergent_tree_registration("add_trees:ancient_emergent_apple", "default:sapling")
ancient_emergent_tree_registration("add_trees:ancient_emergent_acacia", "default:acacia_sapling")
ancient_emergent_tree_registration("add_trees:ancient_emergent_pine", "default:pine_sapling")

local conjoined_tree_registration = function(otpt, node_reci)

	minetest.register_craft({
	output = otpt .."_sapling",
	recipe = {
		{node_reci, node_reci, ""},
		{"", "", ""},
		{"", "", ""},
	}
	})
end

conjoined_tree_registration("add_trees:conjoined_emergent_jungle", "default:emergent_jungle_sapling")
conjoined_tree_registration("add_trees:conjoined_pine", "default:pine_sapling")
conjoined_tree_registration("add_trees:conjoined_aspen", "default:aspen_sapling")
conjoined_tree_registration("add_trees:conjoined_pink_apple", "add_trees:pink_apple_sapling")
local path = minetest.get_modpath("add_trees").."/schematics/"
local chunksize = tonumber(minetest.get_mapgen_setting("chunksize"))
local pine_dirt = "default:dirt_with_snow", "default:dirt_with_coniferous_litter"
local pine_biome = "taiga", "coniferous_forest", "floatland_coniferous_forest"

----------- New trees --------

--- Trees Decoration Registration

local decoration_registration = function(node_name, dirt_type, nb_scale, nb_seed, wh_biome, ymax, ymin, schem, nb_place_offs)

	minetest.register_decoration({
		name = node_name .."_tree",
		deco_type = "schematic",
		place_on = {dirt_type},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = nb_scale,
			spread = {x = 250, y = 250, z = 250},
			seed = nb_seed,
			octaves = 3,
			persist = 0.66
		},
		biomes = {wh_biome},
		y_max = ymax,
		y_min = ymin,
		schematic = path..schem..".mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
		place_offset_y = nb_place_offs,
	})

end

decoration_registration("emergent_apple", "default:dirt_with_grass", 0.0025, 2, "deciduous_forest", 31000, 1, "emergent_apple_tree", 0)
decoration_registration("ancient_emergent_apple", "default:dirt_with_grass", 0.0015, 2, "deciduous_forest", 31000, 1, "ancient_emergent_apple_tree", 0)
decoration_registration("emergent_acacia", "default:dirt_with_dry_grass", 0.0008, 2, "savanna", 31000, 1, "emergent_acacia_tree", 0)
decoration_registration("ancient_emergent_acacia", "default:dirt_with_dry_grass", 0.00008, 2, "savanna", 31000, 1, "ancient_emergent_acacia_tree", 0)
decoration_registration("conjoined_pine", pine_dirt, 0.002, 2, pine_biome, 31000, 4, "conjoined_pine_tree", 0)
decoration_registration("emergent_aspen", "default:dirt_with_grass", 0.00075, 2, "deciduous_forest", 31000, 1, "emergent_aspen_tree", 0)
decoration_registration("conjoined_aspen", "default:dirt_with_grass", 0.0025, 2, "deciduous_forest", 31000, 1, "conjoined_aspen_tree", 0)
decoration_registration("pink_apple", "default:dirt_with_grass", 0.00025, 2, "deciduous_forest", 31000, 1, "pink_apple_tree", 0)
decoration_registration("conjoined_pink_apple", "default:dirt_with_grass", 0.00025, 2, "deciduous_forest", 31000, 1, "conjoined_pink_apple_tree", 0)

if chunksize >= 5 then
	decoration_registration("conjoined_emergent_jungle_", "default:dirt_with_rainforest_litter", 0.00075, 2, "rainforest", 32, 1, "conjoined_emergent_jungle_tree", -4)
	decoration_registration("ancient_emergent_pine", pine_dirt, 0.00023, 2, pine_biome, 31000, 4, "ancient_emergent_pine_tree", -4)
end
	
--- Sapling Growing Definition

local place_schem = function(node_name, descrip, func_grow, position)
	
	local node = minetest.get_node(position)
	if node.name == node_name .."_sapling" then
		minetest.log("action", descrip .." sapling grows into a tree at "..
			minetest.pos_to_string(position))
		minetest.swap_node(position, {name = "air"})
		func_grow(position)
		
	end
end

function add_trees.grow_sapling(pos)
	if not default.can_grow(pos) then
		-- try again 5 min later
		minetest.get_node_timer(pos):start(30)
		return
	end
	place_schem("add_trees:emergent_apple", "An emergent apple", add_trees.grow_emergent_apple_sapling, pos) 
	place_schem("add_trees:ancient_emergent_apple", "An ancient emergent apple", add_trees.grow_ancient_emergent_apple_sapling, pos)
	place_schem("add_trees:conjoined_emergent_jungle", "A conjoined emergent jungle", add_trees.grow_conjoined_emergent_jungle_sapling, pos)
	place_schem("add_trees:emergent_acacia", "An emergent acacia", add_trees.grow_emergent_acacia_sapling, pos)
	place_schem("add_trees:ancient_emergent_acacia", "An ancient emergent acacia", add_trees.grow_ancient_emergent_acacia_sapling, pos)
	place_schem("add_trees:conjoined_pine", "A conjoined pine", add_trees.grow_conjoined_pine_sapling, pos)
	place_schem("add_trees:ancient_emergent_pine", "An ancient emergent pine", add_trees.grow_ancient_emergent_pine_sapling, pos)
	place_schem("add_trees:emergent_aspen", "An emergent aspen", add_trees.grow_emergent_aspen_sapling, pos)
	place_schem("add_trees:conjoined_aspen", "A conjoined aspen", add_trees.grow_conjoined_aspen_sapling, pos)
	place_schem("add_trees:pink_apple", "A pink apple", add_trees.grow_pink_apple_sapling, pos)
	place_schem("add_trees:conjoined_pink_apple", "A conjoined pink apple", add_trees.grow_conjoined_pink_apple_sapling, pos)
	
end

-- Trees Growing Function

function add_trees.grow_emergent_apple_sapling(pos)
	schem = path.."emergent_apple_tree.mts"
	minetest.place_schematic({x = pos.x - 3, y = pos.y, z = pos.z - 3},
		schem, "random", nil, false)
end

function add_trees.grow_ancient_emergent_apple_sapling(pos)
	schem = path.."ancient_emergent_apple_tree.mts"
	minetest.place_schematic({x = pos.x - 5, y = pos.y, z = pos.z - 5},
		schem, "random", nil, false)
end

function add_trees.grow_conjoined_emergent_jungle_sapling(pos)
	schem = path.."conjoined_emergent_jungle_tree.mts"
	minetest.place_schematic({x = pos.x - 12, y = pos.y, z = pos.z - 12},
		schem, "random", nil, false)
end

function add_trees.grow_emergent_acacia_sapling(pos)
	schem = path.."emergent_acacia_tree.mts"
	minetest.place_schematic({x = pos.x - 6, y = pos.y, z = pos.z - 6},
		schem, "random", nil, false)
end

function add_trees.grow_ancient_emergent_acacia_sapling(pos)
	schem = path.."ancient_emergent_acacia_tree.mts"
	minetest.place_schematic({x = pos.x - 12, y = pos.y, z = pos.z - 12},
		schem, "random", nil, false)
end

function add_trees.grow_conjoined_pine_sapling(pos)
	schem = path.."conjoined_pine_tree.mts"
	minetest.place_schematic({x = pos.x - 4, y = pos.y, z = pos.z - 4},
		schem, "random", nil, false)
end

function add_trees.grow_ancient_emergent_pine_sapling(pos)
	schem = path.."ancient_emergent_pine_tree.mts"
	minetest.place_schematic({x = pos.x - 15, y = pos.y, z = pos.z - 15},
		schem, "random", nil, false)
end

function add_trees.grow_emergent_aspen_sapling(pos)
	schem = path.."emergent_aspen_tree.mts"
	minetest.place_schematic({x = pos.x - 2, y = pos.y, z = pos.z - 2},
		schem, "random", nil, false)
end

function add_trees.grow_conjoined_aspen_sapling(pos)
	schem = path.."conjoined_aspen_tree.mts"
	minetest.place_schematic({x = pos.x - 3, y = pos.y, z = pos.z - 3},
		schem, "random", nil, false)
end

function add_trees.grow_pink_apple_sapling(pos)
	schem = path.."pink_apple_tree.mts"
	minetest.place_schematic({x = pos.x - 2, y = pos.y, z = pos.z - 2},
		schem, "random", nil, false)
end

function add_trees.grow_conjoined_pink_apple_sapling(pos)
	schem = path.."conjoined_pink_apple_tree.mts"
	minetest.place_schematic({x = pos.x - 4, y = pos.y, z = pos.z - 4},
		schem, "random", nil, false)
end
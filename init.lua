add_trees = {}

local path = minetest.get_modpath("add_trees")
dofile(path.."/trees.lua")
dofile(path.."/nodes.lua")
dofile(path.."/crafts.lua")